import { Component } from '@angular/core';

@Component({
  selector: 'app-layout',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class AuthLayoutComponent {}
