import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-pembayaran-list',
  templateUrl: './pembayaran-list.component.html',
  animations: [fadeInOutTranslate]
})

export class PembayaranListComponent implements OnInit {
  public loading = false;
  private pembayaran_url = '/api/pembayarans';

  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  rowsFilter = [];
  tempFilter = [];

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {
      // console.log(data);
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
  }


  getData(cb) {

    const pembayaran_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.pembayaran_url, pembayaran_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg['result']);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {}

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.periode.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }



}
