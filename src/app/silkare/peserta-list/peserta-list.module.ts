import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaListComponent } from './peserta-list.component';
import { PesertaDetailComponent } from '../peserta-detail/peserta-detail.component';
import { PesertaEditComponent } from '../peserta-edit/peserta-edit.component';
import { PesertaListRoutes } from './peserta-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PesertaListComponent, PesertaDetailComponent, PesertaEditComponent]
})

export class PesertaListModule {}
