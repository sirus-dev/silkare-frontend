import { Routes } from '@angular/router';

import { PaketEditComponent } from './paket-edit.component';

export const PaketEditRoutes: Routes = [{
  path: '',
  component: PaketEditComponent,
  data: {
    breadcrumb: 'Ubah Paket',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
