import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PaketEditComponent } from './paket-edit.component';
import { PaketEditRoutes } from './paket-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PaketEditRoutes),
      SharedModule
  ],
  declarations: [PaketEditComponent]
})

export class PaketEditModule {}
