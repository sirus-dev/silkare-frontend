import { Routes } from '@angular/router';

import { PerusahaanReportComponent } from './perusahaan-report.component';

export const PerusahaanReportRoutes: Routes = [{
  path: '',
  component: PerusahaanReportComponent,
  data: {
    breadcrumb: 'Laporan Perusahaan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
