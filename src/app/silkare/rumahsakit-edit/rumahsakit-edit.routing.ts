import { Routes } from '@angular/router';

import { RumahSakitEditComponent } from './rumahsakit-edit.component';

export const RumahSakitEditRoutes: Routes = [{
  path: '',
  component: RumahSakitEditComponent,
  data: {
    breadcrumb: 'Ubah Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
