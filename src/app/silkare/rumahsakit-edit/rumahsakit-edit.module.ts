import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitEditComponent } from './rumahsakit-edit.component';
import { RumahSakitEditRoutes } from './rumahsakit-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitEditRoutes),
      SharedModule
  ],
  declarations: [RumahSakitEditComponent]
})

export class RumahSakitEditModule {}
