import { Routes } from '@angular/router';

import { PaketListComponent } from './paket-list.component';

export const PaketListRoutes: Routes = [{
  path: '',
  component: PaketListComponent,
  data: {
    breadcrumb: 'Daftar Paket',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
