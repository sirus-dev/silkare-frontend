import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PaketDetailComponent } from './paket-detail.component';
import { PaketDetailRoutes } from './paket-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PaketDetailRoutes),
      SharedModule
  ],
  declarations: [PaketDetailComponent]
})

export class PaketEditModule {}
