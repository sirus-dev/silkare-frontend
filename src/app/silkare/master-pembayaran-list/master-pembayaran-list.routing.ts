import { Routes } from '@angular/router';

import { MasterPembayaranListComponent } from './master-pembayaran-list.component';

export const MasterPembayaranListRoutes: Routes = [{
  path: '',
  component: MasterPembayaranListComponent,
  data: {
    breadcrumb: 'Daftar Master Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
