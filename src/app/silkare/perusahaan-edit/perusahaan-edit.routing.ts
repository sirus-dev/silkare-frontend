import { Routes } from '@angular/router';

import { PerusahaanEditComponent } from './perusahaan-edit.component';

export const PerusahaanEditRoutes: Routes = [{
  path: '',
  component: PerusahaanEditComponent,
  data: {
    breadcrumb: 'Ubah Perusahaan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
