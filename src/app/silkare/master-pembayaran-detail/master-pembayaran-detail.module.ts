import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MasterPembayaranDetailComponent } from './master-pembayaran-detail.component';
import { MasterPembayaranDetailRoutes } from './master-pembayaran-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MasterPembayaranDetailRoutes),
      SharedModule
  ],
  declarations: [MasterPembayaranDetailComponent]
})

export class MasterPembayaranEditModule {}
