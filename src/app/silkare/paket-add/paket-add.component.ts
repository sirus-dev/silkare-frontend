import {Component, OnInit} from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-paket-add',
  templateUrl: './paket-add.component.html'
})

export class PaketAddComponent implements OnInit {
  public loading = false;
  private paket_url = '/api/pakets';
  private perusahaan_url = '/api/perusahaans';
  private klinik_url = '/api/kliniks';
  private rumahsakit_url = '/api/rumahsakits';
  private role_url = '/api/pakets-permissions/roles';

  public data_roles;
  public data_perusahaan;
  public data_klinik;
  public data_rumahsakit;
  public show_perusahaan = false;
  public show_klinik = false;
  public show_rumahsakit = false;

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private router: Router) {

    const txt_paketname = new FormControl('', [Validators.required]);
    const txt_email = new FormControl('', [Validators.required, Validators.email]);
    const txt_password = new FormControl('', Validators.required);
    const txt_role = new FormControl('', Validators.required);
    const txt_perusahaan = new FormControl();
    const txt_klinik = new FormControl();
    const txt_rumahsakit = new FormControl();

    this.myForm = new FormGroup({
      paketname: txt_paketname,
      email: txt_email,
      password: txt_password,
      role: txt_role,
      perusahaan: txt_perusahaan,
      klinik: txt_klinik,
      rumahsakit: txt_rumahsakit
    });
  }

  ngOnInit() {
    this.getRole();
  }


  getRole() {

    this.loading = true;
    const role_json = {};
    this.httpRequest.httpGet(this.role_url, role_json).subscribe(
      result => {
        try {
          const res = JSON.parse(result._body);
          this.data_roles = res['roles'];
          this.loading = false;
          // console.log(this.data_roles);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getPerusahaan() {

    this.loading = true;
    const perusahaan_json = {};
    this.httpRequest.httpGet(this.perusahaan_url, perusahaan_json).subscribe(
      result => {
        try {
          const res = JSON.parse(result._body);
          this.data_roles = res['roles'];
          this.loading = false;
          // console.log(this.data_roles);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  // requestAutocompleteItems = (text: string): Observable<Response> => {
  //   const url = `https://api.github.com/search/repositories?q=${text}`;
  //   // return this.http
  //   //     .get(url)
  //   //     .map(data => data.json().items.map(item => item.full_name));

  //   const perusahaan_json = {};
  //   this.httpRequest.httpGet(this.perusahaan_url, perusahaan_json).subscribe(
  //     result => {
  //       try {
  //         const res = JSON.parse(result._body);
  //         this.data_roles = res['roles'];
  //         this.loading = false;
  //         // console.log(this.data_roles);

  //       } catch (error) {
  //         this.loading = false;
  //         this.errorMessage.openErrorSwal('Something wrong.');
  //       }
  //     },
  //     error => {
  //       console.log(error);
  //       this.loading = false;
  //     }
  //   );
  // }

  changeRole(event) {
    const val = event.target.value.toLowerCase();
    const text = event.target.options[event.target.selectedIndex].text;

    console.log(text);
    if (text === 'Perusahaan') {
      this.show_perusahaan = true;
      this.show_klinik = false;
      this.show_rumahsakit = false;
    }else if (text === 'Klinik') {
      this.show_perusahaan = false;
      this.show_klinik = true;
      this.show_rumahsakit = false;
    }else if (text === 'Rumah Sakit') {
      this.show_perusahaan = false;
      this.show_klinik = false;
      this.show_rumahsakit = true;
    }else {
      this.show_perusahaan = false;
      this.show_klinik = false;
      this.show_rumahsakit = false;
    }
  }

  onSubmit() {
    this.submitted = true;
    const paket_val = this.myForm.value;

    paket_val['confirmed'] = false;
    paket_val['blocked'] = false;
    paket_val['provider'] = 'local';
    const paket_json = JSON.stringify(paket_val);
    // console.log(paket_json);

    this.loading = true;
    this.httpRequest.httpPost(this.paket_url, paket_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.router.navigate(['/silkare/paket-access/paket-list']);
          this.errorMessage.openSuccessSwal('Paket created successfully.');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }



}
