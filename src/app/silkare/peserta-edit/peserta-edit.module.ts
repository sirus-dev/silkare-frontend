import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaEditComponent } from './peserta-edit.component';
import { PesertaEditRoutes } from './peserta-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaEditRoutes),
      SharedModule
  ],
  declarations: [PesertaEditComponent]
})

export class PesertaEditModule {}
