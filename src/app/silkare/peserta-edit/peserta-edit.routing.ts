import { Routes } from '@angular/router';

import { PesertaEditComponent } from './peserta-edit.component';

export const PesertaEditRoutes: Routes = [{
  path: '',
  component: PesertaEditComponent,
  data: {
    breadcrumb: 'Ubah Peserta',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
