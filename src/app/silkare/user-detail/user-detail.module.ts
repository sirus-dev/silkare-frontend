import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UserDetailComponent } from './user-detail.component';
import { UserDetailRoutes } from './user-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UserDetailRoutes),
      SharedModule
  ],
  declarations: [UserDetailComponent]
})

export class UserEditModule {}
