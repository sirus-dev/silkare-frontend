import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikEditComponent } from './klinik-edit.component';
import { KlinikEditRoutes } from './klinik-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikEditRoutes),
      SharedModule
  ],
  declarations: [KlinikEditComponent]
})

export class KlinikEditModule {}
