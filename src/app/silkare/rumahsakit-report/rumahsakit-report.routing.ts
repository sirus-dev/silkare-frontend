import { Routes } from '@angular/router';

import { RumahSakitReportComponent } from './rumahsakit-report.component';

export const RumahSakitReportRoutes: Routes = [{
  path: '',
  component: RumahSakitReportComponent,
  data: {
    breadcrumb: 'Laporan Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
