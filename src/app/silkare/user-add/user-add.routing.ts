import { Routes } from '@angular/router';

import { UserAddComponent } from './user-add.component';

export const UserAddRoutes: Routes = [{
  path: '',
  component: UserAddComponent,
  data: {
    breadcrumb: 'Tambah User',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
