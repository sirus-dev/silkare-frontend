import { Routes } from '@angular/router';

import { KlinikAddComponent } from './klinik-add.component';

export const KlinikAddRoutes: Routes = [{
  path: '',
  component: KlinikAddComponent,
  data: {
    breadcrumb: 'Tambah Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
