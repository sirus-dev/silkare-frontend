import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikAddComponent } from './klinik-add.component';
import { KlinikAddRoutes } from './klinik-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikAddRoutes),
      SharedModule
  ],
  declarations: [KlinikAddComponent]
})

export class KlinikAddModule {}
