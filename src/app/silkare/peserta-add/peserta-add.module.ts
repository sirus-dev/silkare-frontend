import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaAddComponent } from './peserta-add.component';
import { PesertaAddRoutes } from './peserta-add.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaAddRoutes),
      SharedModule
  ],
  declarations: [PesertaAddComponent]
})

export class PesertaAddModule {}
