import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitListComponent } from './rumahsakit-list.component';
import { RumahSakitDetailComponent } from '../rumahsakit-detail/rumahsakit-detail.component';
import { RumahSakitEditComponent } from '../rumahsakit-edit/rumahsakit-edit.component';
import { RumahSakitListRoutes } from './rumahsakit-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [RumahSakitListComponent, RumahSakitDetailComponent, RumahSakitEditComponent]
})

export class RumahSakitListModule {}
