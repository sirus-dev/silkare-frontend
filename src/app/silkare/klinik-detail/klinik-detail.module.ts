import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikDetailComponent } from './klinik-detail.component';
import { KlinikDetailRoutes } from './klinik-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikDetailRoutes),
      SharedModule
  ],
  declarations: [KlinikDetailComponent]
})

export class KlinikEditModule {}
