import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikListComponent } from './klinik-list.component';
import { KlinikDetailComponent } from '../klinik-detail/klinik-detail.component';
import { KlinikEditComponent } from '../klinik-edit/klinik-edit.component';
import { KlinikListRoutes } from './klinik-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [KlinikListComponent, KlinikDetailComponent, KlinikEditComponent]
})

export class KlinikListModule {}
