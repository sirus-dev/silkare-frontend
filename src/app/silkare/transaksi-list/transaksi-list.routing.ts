import { Routes } from '@angular/router';

import { TransaksiListComponent } from './transaksi-list.component';

export const TransaksiListRoutes: Routes = [{
  path: '',
  component: TransaksiListComponent,
  data: {
    breadcrumb: 'Daftar Transaksi',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
