import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UserEditComponent } from './user-edit.component';
import { UserEditRoutes } from './user-edit.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UserEditRoutes),
      SharedModule
  ],
  declarations: [UserEditComponent]
})

export class UserEditModule {}
