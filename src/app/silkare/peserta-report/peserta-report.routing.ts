import { Routes } from '@angular/router';

import { PesertaReportComponent } from './peserta-report.component';

export const PesertaReportRoutes: Routes = [{
  path: '',
  component: PesertaReportComponent,
  data: {
    breadcrumb: 'Laporan Peserta',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
