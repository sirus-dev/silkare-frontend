import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { UserListComponent } from './user-list.component';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserListRoutes } from './user-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UserListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [UserListComponent, UserDetailComponent, UserEditComponent]
})

export class UserListModule {}
