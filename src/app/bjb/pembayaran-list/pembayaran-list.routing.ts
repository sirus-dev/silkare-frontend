import { Routes } from '@angular/router';

import { PembayaranListComponent } from './pembayaran-list.component';

export const PembayaranListRoutes: Routes = [{
  path: '',
  component: PembayaranListComponent,
  data: {
    breadcrumb: 'Daftar Pembayaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
