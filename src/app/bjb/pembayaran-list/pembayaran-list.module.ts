import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PembayaranListComponent } from './pembayaran-list.component';
import { PembayaranListRoutes } from './pembayaran-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PembayaranListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PembayaranListComponent]
})

export class PembayaranListModule {}
