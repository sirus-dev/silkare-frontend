import { Routes } from '@angular/router';

import { RumahSakitDetailComponent } from './rumahsakit-detail.component';

export const RumahSakitDetailRoutes: Routes = [{
  path: '',
  component: RumahSakitDetailComponent,
  data: {
    breadcrumb: 'Detail Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
