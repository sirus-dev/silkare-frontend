import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-perusahaan-list',
  templateUrl: './perusahaan-list.component.html',
  animations: [fadeInOutTranslate]
})

export class PerusahaanListComponent implements OnInit {
  public loading = false;
  private perusahaan_url = '/api/perusahaans';

  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  rowsFilter = [];
  tempFilter = [];

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
  }

  getData(cb) {

    const perusahaan_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.perusahaan_url + '?is_deleted=false', perusahaan_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  deleteData(id) {
    const perusahaan_val = {};
    perusahaan_val['_id'] = id;
    perusahaan_val['deleted'] = true;
    const perusahaan_json = JSON.stringify(perusahaan_val);
    // console.log(perusahaan_json);

    this.loading = true;
    this.httpRequest.httpPut(this.perusahaan_url + '/' + id, perusahaan_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.getData((data) => {
            this.tempFilter = [...data];
            this.rowsFilter = data;
          });

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.nama_perusahaan.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  updateStatus(event) {
    const val = event.target.value.toLowerCase();

    console.log(val);
    const temp = this.tempFilter.filter(function(d) {
      return d.role.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }


  showConfirmDelete(event, id) {
    swal({
      title: 'Konfirmasi Hapus',
      text: 'Anda yakin menghapus perusahaan ini?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteData(id);
        swal(
            'Info!',
            'User berhasil dihapus.',
            'success'
        );
      }
    });
  }


  showDetail() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowDetail').click();
    }.bind(this), 2000);
  }

  showEdit() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowEdit').click();
    }.bind(this), 2000);
  }


}
