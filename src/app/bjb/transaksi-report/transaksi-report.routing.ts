import { Routes } from '@angular/router';

import { TransaksiReportComponent } from './transaksi-report.component';

export const TransaksiReportRoutes: Routes = [{
  path: '',
  component: TransaksiReportComponent,
  data: {
    breadcrumb: 'Laporan Iuran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
