import { Routes } from '@angular/router';

import { KlinikDetailComponent } from './klinik-detail.component';

export const KlinikDetailRoutes: Routes = [{
  path: '',
  component: KlinikDetailComponent,
  data: {
    breadcrumb: 'Detail Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
