import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TransaksiConfirmComponent } from './transaksi-confirm.component';
import { TransaksiConfirmRoutes } from './transaksi-confirm.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TransaksiConfirmRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [TransaksiConfirmComponent]
})

export class TransaksiConfirmModule {}
