import {Component, OnInit} from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConverterService } from '../../shared/service/converter.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-transaksi-confirm',
  templateUrl: './transaksi-confirm.component.html'
})

export class TransaksiConfirmComponent implements OnInit {

  public loading = false;
  private transaksi_inquiry = '/api/transaksis/inquiry';
  private transaksi_confirm = '/api/transaksis/confirm';

  public data_inquiry = {};
  public data_confirm = {};

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private convert: ConverterService) {

    const txt_kode_perusahaan = new FormControl('', [Validators.required]);
    const txt_periode = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      kode_perusahaan: txt_kode_perusahaan,
      periode: txt_periode
    });

  }


  ngOnInit() {
    // console.log(this.data_inquiry['result']);
  }



  onSubmit() {
    this.submitted = true;
    this.loading = true;
    const transaksi_inquiry_val = this.myForm.value;
    const transaksi_inquiry_json = JSON.stringify(transaksi_inquiry_val);
    // console.log(transaksi_inquiry_json);

    this.loading = true;
    this.httpRequest.httpPost(this.transaksi_inquiry, transaksi_inquiry_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.data_inquiry = result_msg;
          // console.log(result_msg);

          let msg = '';
          this.loading = false;
          if (result_msg['result']['perusahaan']['_id']) {
            msg = msg + 'Data Perusahaan ditemukan.<br/>';
          }else {
            msg = msg + 'Data Perusahaan <u>tidak</u> ditemukan.<br/>';
          }

          if (result_msg['result']['transaksi']['_id']) {
            msg = msg + 'Data Transaksi ditemukan.<br/>';
          }else {
            msg = msg + 'Data Transaksi <u>tidak</u> ditemukan.<br/>';
          }

          if (result_msg['result']['peserta'].length > 0) {
            msg = msg + 'Data Anggota ditemukan.<br/>';
          }else {
            msg = msg + 'Data Anggota <u>tidak</u> ditemukan.<br/>';
          }
          this.errorMessage.openSuccessSwal(msg);
          this.loading = false;
          // this.router.navigate(['/silkare/user-access/user-list']);
          // this.errorMessage.openSuccessSwal('Data ditemukan.');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  onConfirm(status) {
    this.submitted = true;
    let transaksi_confirm_val = {};
    transaksi_confirm_val = this.myForm.value;
    transaksi_confirm_val['jumlah_bayar'] = this.data_inquiry['result']['transaksi']['jumlah_bayar'];
    transaksi_confirm_val['status'] = status;
    const transaksi_confirm_json = JSON.stringify(transaksi_confirm_val);
    // console.log(transaksi_confirm_json);

    this.loading = true;
    this.httpRequest.httpPost(this.transaksi_confirm, transaksi_confirm_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.data_confirm = result_msg;
          // console.log(result_msg);

          this.loading = false;
          this.errorMessage.openSuccessSwal(this.data_confirm['message']);
          this.router.navigate(['/bjb/transaksi/transaksi-list']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  showConfirmDelete(event, status) {
    swal({
      title: 'Konfirmasi Transaksi',
      text: 'Anda yakin melakukan konfirmasi transaksi ini?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.onConfirm(status);
      }
    });
  }

}
