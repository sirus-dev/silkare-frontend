import { Routes } from '@angular/router';

import { TransaksiConfirmComponent } from './transaksi-confirm.component';

export const TransaksiConfirmRoutes: Routes = [{
  path: '',
  component: TransaksiConfirmComponent,
  data: {
    breadcrumb: 'Konfirmasi Iuran',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
