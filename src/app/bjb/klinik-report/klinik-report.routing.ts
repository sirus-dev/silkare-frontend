import { Routes } from '@angular/router';

import { KlinikReportComponent } from './klinik-report.component';

export const KlinikReportRoutes: Routes = [{
  path: '',
  component: KlinikReportComponent,
  data: {
    breadcrumb: 'Laporan Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
