import { Routes } from '@angular/router';

import { BlankComponent } from './blank.component';

export const BlankRoutes: Routes = [{
  path: '',
  component: BlankComponent,
  data: {
    breadcrumb: 'Blank',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
