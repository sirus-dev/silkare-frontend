import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BlankComponent } from './blank.component';
import { BlankRoutes } from './blank.routing';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(BlankRoutes),
      SharedModule
  ],
  declarations: [BlankComponent]
})

export class BlankModule {}
