import { Routes } from '@angular/router';

import { KlinikListComponent } from './klinik-list.component';

export const KlinikListRoutes: Routes = [{
  path: '',
  component: KlinikListComponent,
  data: {
    breadcrumb: 'Daftar Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
