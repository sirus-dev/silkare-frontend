import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanListComponent } from './perusahaan-list.component';
import { PerusahaanDetailComponent } from '../perusahaan-detail/perusahaan-detail.component';
import { PerusahaanListRoutes } from './perusahaan-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanListRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PerusahaanListComponent, PerusahaanDetailComponent]
})

export class PerusahaanListModule {}
