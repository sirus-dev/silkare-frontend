import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanDetailComponent } from './perusahaan-detail.component';
import { PerusahaanDetailRoutes } from './perusahaan-detail.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanDetailRoutes),
      SharedModule
  ],
  declarations: [PerusahaanDetailComponent]
})

export class PerusahaanEditModule {}
