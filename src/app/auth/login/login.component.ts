import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public loading = false;
  private login_url = '/api/auth/local';

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private session: SessionService,
    private router: Router) {
    const txt_identifier = new FormControl('', [
      Validators.required,
      Validators.email
    ]);
    const txt_password = new FormControl('', Validators.required);

    this.myForm = new FormGroup({
      identifier: txt_identifier,
      password: txt_password
    });
  }

  ngOnInit() {

    // const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibm'+
        // 'FtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1NDQ3NTY3MzN9.'+
        // 'S1hxDgD7An-4xAl7wg9EmZqe92UXr_mk8zYOJns4tNU';
    // const obj = {'user': {'role': {'name': 'klinik'}}};

    // this.session.logIn(jwt, JSON.stringify(obj));
    // console.log(this.session.checkAccessButton(this.router.url));
    // console.log(this.session.isLoggedIn());
  }

  onSubmit() {
    this.submitted = true;
    const login_val = this.myForm.value;
    const login_json = JSON.stringify(login_val);

    this.loading = true;
    this.httpRequest.httpLogin(this.login_url, login_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let role = result_msg['user']['role']['type'];
          role = role.toString().toLowerCase();
          role = role.replace('_', '-');
          // console.log(result_msg);
          // console.log(result_msg['user']);

          this.loading = false;
          this.session.logIn(result_msg['jwt'], JSON.stringify(result_msg['user']));
          this.router.navigate(['/' + role]);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
}
