import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import { MenuItems } from './menu/menu-items/menu-items';
import { MenuItemsSilkare } from './menu/menu-items-silkare/menu-items-silkare';
import { MenuItemsPerusahaan } from './menu/menu-items-perusahaan/menu-items-perusahaan';
import { MenuItemsRumahSakit } from './menu/menu-items-rumah-sakit/menu-items-rumah-sakit';
import { MenuItemsKlinik } from './menu/menu-items-klinik/menu-items-klinik';
import { MenuItemsBJB } from './menu/menu-items-bjb/menu-items-bjb';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import { CardComponent } from './card/card.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ParentRemoveDirective} from './elements/parent-remove.directive';
import {PaginationModule} from 'ngx-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SpinnerComponent} from './spinner/spinner.component';
import {GoTopButtonModule} from 'ng2-go-top-button';
import {ScrollModule} from './scroll/scroll.module';
import {ToastyModule} from 'ng2-toasty';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {TagInputModule} from 'ngx-chips';
import {AgmCoreModule} from '@agm/core';
import {Ng2GoogleChartsModule} from 'ng2-google-charts';
import {ClickOutsideModule} from 'ng-click-outside';
import {HttpRequestService} from './service/http-request.service';
import {ErrorMessageService} from './service/error-message.service';
import {SessionService} from './service/session.service';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ConverterService} from './service/converter.service';
import {ModalBasicComponent} from './modal-basic/modal-basic.component';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      PaginationModule.forRoot(),
      ScrollModule,
      ToastyModule.forRoot(),
      SimpleNotificationsModule.forRoot(),
      TagInputModule,
      AgmCoreModule.forRoot({apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk'}),
      Ng2GoogleChartsModule,
      ClickOutsideModule,
      GoTopButtonModule,
      NgxDatatableModule
  ],
  declarations: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent,
      ModalBasicComponent
  ],
  exports: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent,
      NgbModule,
      PaginationModule,
      FormsModule,
      ReactiveFormsModule,
      ScrollModule,
      ToastyModule,
      SimpleNotificationsModule,
      TagInputModule,
      AgmCoreModule,
      Ng2GoogleChartsModule,
      ClickOutsideModule,
      GoTopButtonModule,
      NgxDatatableModule,
      ModalBasicComponent
  ],
  providers: [
      MenuItems,
      MenuItemsSilkare,
      MenuItemsPerusahaan,
      MenuItemsRumahSakit,
      MenuItemsKlinik,
      MenuItemsBJB,
      HttpRequestService,
      ErrorMessageService,
      SessionService,
      ConverterService
  ]
})
export class SharedModule { }
