import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMSPERUSAHAAN = [
  {
    label: '',
    main: [
      {
        main_state : 'perusahaan',
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        main_state : 'perusahaan',
        state: 'profile',
        name: 'Profil Pemberi Kerja',
        type: 'link',
        icon: 'icofont icofont-certificate-alt-1'
      },
      {
        main_state : 'perusahaan',
        state: 'peserta',
        name: 'Anggota',
        type: 'sub',
        icon: 'icofont icofont-user-alt-3',
        children: [
          {
            state: 'peserta-list',
            name: 'Data Anggota'
          },
          {
            state: 'peserta-add',
            name: 'Tambah Anggota'
          },
          {
            state: 'peserta-report',
            name: 'Laporan Anggota'
          }
        ]
      },
      {
        main_state : 'perusahaan',
        state: 'rumahsakit',
        name: 'Rumah Sakit',
        type: 'sub',
        icon: 'icofont icofont-hospital',
        children: [
          {
            state: 'rumahsakit-list',
            name: 'Info Rumah Sakit'
          },
        ]
      },
      {
        main_state : 'perusahaan',
        state: 'klinik',
        name: 'FKTP',
        type: 'sub',
        icon: 'icofont icofont-medical-sign-alt',
        children: [
          {
            state: 'klinik-list',
            name: 'Info FKTP'
          },
        ]
      },
      {
        main_state : 'perusahaan',
        state: 'transaksi',
        name: 'Iuran',
        type: 'sub',
        icon: 'icofont icofont-retweet',
        children: [
          {
            state: 'transaksi-list',
            name: 'Data Iuran'
          },
          {
            state: 'transaksi-add',
            name: 'Tambah Iuran'
          },
          {
            state: 'transaksi-report',
            name: 'Laporan Iuran'
          }
        ]
      },
    ]
  },
];

@Injectable()
export class MenuItemsPerusahaan {
  getAll(): Menu[] {
    return MENUITEMSPERUSAHAAN;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
