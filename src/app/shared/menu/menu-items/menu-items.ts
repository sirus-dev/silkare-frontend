import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: '',
    main: [
      {
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        state: 'perusahaan',
        name: 'Perusahaan',
        type: 'sub',
        icon: 'icofont icofont-building',
        children: [
          {
            state: 'perusahaan-list',
            name: 'Lihat Data Perusahaan'
          },
          {
            state: 'perusahaan-add',
            name: 'Tambah Data Perusahaan'
          },
          {
            state: 'perusahaan-report',
            name: 'Laporan Data Perusahaan'
          }
        ]
      },
      {
        state: 'rumahsakit',
        name: 'Rumah Sakit',
        type: 'sub',
        icon: 'icofont icofont-hospital',
        children: [
          {
            state: 'rumahsakit-list',
            name: 'Lihat Data Rumah Sakit'
          },
          {
            state: 'rumahsakit-add',
            name: 'Tambah Data Rumah Sakit'
          },
          {
            state: 'rumahsakit-report',
            name: 'Laporan Data Rumah Sakit'
          }
        ]
      },
      {
        state: 'klinik',
        name: 'Klinik',
        type: 'sub',
        icon: 'icofont icofont-medical-sign-alt',
        children: [
          {
            state: 'klinik-list',
            name: 'Lihat Data Klinik'
          },
          {
            state: 'klinik-add',
            name: 'Tambah Data Klinik'
          },
          {
            state: 'klinik-report',
            name: 'Laporan Data Klinik'
          }
        ]
      },
      {
        state: 'bjb',
        name: 'BJB',
        type: 'sub',
        icon: 'icofont icofont-man-in-glasses',
        children: [
          {
            state: 'bjb-list',
            name: 'Lihat Data BJB'
          },
          {
            state: 'bjb-add',
            name: 'Tambah Data BJB'
          },
          {
            state: 'bjb-report',
            name: 'Laporan Data BJB'
          }
        ]
      },
      {
        state: 'authentication',
        name: 'Authentication',
        type: 'sub',
        icon: 'ti-id-badge',
        children: [
          {
            state: 'login',
            type: 'link',
            name: 'Login',
            target: true
          },
          {
            state: 'forgot',
            name: 'Forgot Password',
            target: true
          },
          {
            state: 'lock-screen',
            name: 'Lock Screen',
            target: true
          },
        ]
      },
      {
        state: 'error',
        external: 'http://lite.codedthemes.com/flatable/error.html',
        name: 'Error',
        type: 'external',
        icon: 'ti-layout-list-post',
        target: true
      },
    ]
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
