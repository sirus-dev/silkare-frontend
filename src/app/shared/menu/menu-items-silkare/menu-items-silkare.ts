import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMSSilkare = [
  {
    label: '',
    main: [
      {
        main_state : 'silkare',
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        main_state : 'silkare',
        state: 'profile',
        name: 'Profil Patient Funding',
        type: 'link',
        icon: 'icofont icofont-certificate-alt-1'
      },
      {
        main_state : 'silkare',
        state: 'transaksi',
        name: 'Iuran',
        type: 'sub',
        icon: 'icofont icofont-retweet',
        children: [
          {
            state: 'transaksi-list',
            name: 'Data Iuran'
          },
          {
            state: 'transaksi-report',
            name: 'Laporan Iuran'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'pembayaran',
        name: 'Pembayaran',
        type: 'sub',
        icon: 'icofont icofont-cur-dollar',
        children: [
          {
            state: 'pembayaran-list',
            name: 'Data Pembayaran'
          },
          {
            state: 'pembayaran-add',
            name: 'Proses Pembayaran'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'perusahaan',
        name: 'Pemberi Kerja',
        type: 'sub',
        icon: 'icofont icofont-building',
        children: [
          {
            state: 'perusahaan-list',
            name: 'Data Pemberi Kerja'
          },
          {
            state: 'perusahaan-add',
            name: 'Tambah Pemberi Kerja'
          },
          {
            state: 'perusahaan-report',
            name: 'Laporan Pemberi Kerja'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'rumahsakit',
        name: 'Rumah Sakit',
        type: 'sub',
        icon: 'icofont icofont-hospital',
        children: [
          {
            state: 'rumahsakit-list',
            name: 'Data Rumah Sakit'
          },
          {
            state: 'rumahsakit-add',
            name: 'Tambah Rumah Sakit'
          },
          {
            state: 'rumahsakit-report',
            name: 'Laporan Rumah Sakit'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'klinik',
        name: 'FKTP',
        type: 'sub',
        icon: 'icofont icofont-medical-sign-alt',
        children: [
          {
            state: 'klinik-list',
            name: 'Data FKTP'
          },
          {
            state: 'klinik-add',
            name: 'Tambah FKTP'
          },
          {
            state: 'klinik-report',
            name: 'Laporan FKTP'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'peserta',
        name: 'Anggota',
        type: 'sub',
        icon: 'icofont icofont-user-alt-3',
        children: [
          {
            state: 'peserta-list',
            name: 'Data Anggota'
          },
          {
            state: 'peserta-add',
            name: 'Tambah Anggota'
          },
          {
            state: 'peserta-report',
            name: 'Laporan Anggota'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'user-access',
        name: 'User Access',
        type: 'sub',
        icon: 'icofont icofont-key',
        children: [
          {
            state: 'user-list',
            name: 'Data User Access'
          },
          {
            state: 'user-add',
            name: 'Tambah User Access'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'paket',
        name: 'Paket',
        type: 'sub',
        icon: 'icofont icofont-box',
        children: [
          {
            state: 'paket-list',
            name: 'Data Paket'
          },
          {
            state: 'paket-add',
            name: 'Tambah Paket'
          }
        ]
      },
      {
        main_state : 'silkare',
        state: 'master-pembayaran',
        name: 'Master Pembayaran',
        type: 'sub',
        icon: 'icofont icofont-gear',
        children: [
          {
            state: 'master-pembayaran-list',
            name: 'Data Master Pembayaran'
          },
          {
            state: 'master-pembayaran-add',
            name: 'Tambah Master Pembayaran'
          }
        ]
      },
    ]
  },
];

@Injectable()
export class MenuItemsSilkare {
  getAll(): Menu[] {
    return MENUITEMSSilkare;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
