import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMSRUMAHSAKIT = [
  {
    label: '',
    main: [
      {
        main_state : 'rumah-sakit',
        state: 'dashboard',
        name: 'Dashboard',
        type: 'link',
        icon: 'icofont icofont-home'
      },
      {
        main_state : 'rumah-sakit',
        state: 'profile',
        name: 'Profil Rumah Sakit',
        type: 'link',
        icon: 'icofont icofont-certificate-alt-1'
      },
      {
        main_state : 'rumah-sakit',
        state: 'perusahaan',
        name: 'Pemberi Kerja',
        type: 'sub',
        icon: 'icofont icofont-building',
        children: [
          {
            state: 'perusahaan-list',
            name: 'Info Pemberi Kerja'
          },
        ]
      },
      {
        main_state : 'rumah-sakit',
        state: 'klinik',
        name: 'FKTP',
        type: 'sub',
        icon: 'icofont icofont-medical-sign-alt',
        children: [
          {
            state: 'klinik-list',
            name: 'Info FKTP'
          },
        ]
      },
      {
        main_state : 'rumah-sakit',
        state: 'peserta',
        name: 'Anggota',
        type: 'sub',
        icon: 'icofont icofont-user-alt-3',
        children: [
          {
            state: 'peserta-list',
            name: 'Info Anggota'
          },
        ]
      },
    ]
  },
];

@Injectable()
export class MenuItemsRumahSakit {
  getAll(): Menu[] {
    return MENUITEMSRUMAHSAKIT;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
