import {Component, OnInit} from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { ConverterService } from '../../shared/service/converter.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-transaksi-add',
  templateUrl: './transaksi-add.component.html'
})

export class TransaksiAddComponent implements OnInit {
  public loading = false;
  private peserta_url = '/api/pesertas';
  private transaksi_url = '/api/penagihans';
  private transaksidetail_url = '/api/penagihandetails';

  public data_roles =  [];
  public data_peserta =  [];
  public rowsFilter =  [];
  public rowsFix =  [];
  public numPeserta = 0;
  public sumBayar = 0;

  public id_perusahaan: string = '5c1092e408950336e0982829';
  public kode_perusahaan: string = '';
  public nama_perusahaan: string = '';
  public jumlah_bayar: string = '';

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    private router: Router,
    private fb: FormBuilder) {

    const txt_kode_perusahaan = new FormControl('', Validators.required);
    const txt_nama_perusahaan = new FormControl('', Validators.required);
    const txt_tanggal_bayar = new FormControl('', Validators.required);
    const txt_jumlah_bayar = new FormControl('', Validators.required);
    const txt_periode = new FormControl({ year: new Date().getFullYear(), month: new Date().getMonth() + 1 }, Validators.required);

    this.myForm = new FormGroup({
      kode_perusahaan: txt_kode_perusahaan,
      nama_perusahaan: txt_nama_perusahaan,
      periode: txt_periode,
      tanggal_bayar: txt_tanggal_bayar,
      jumlah_bayar: txt_jumlah_bayar,
    });
  }

  ngOnInit() {
    this.kode_perusahaan = 'P0001';
    this.nama_perusahaan = 'Perusahaan 1';
  }

  onSubmit() {
    this.submitted = true;
    const transaksi_val = this.myForm.value;

    transaksi_val['perusahaan'] = this.id_perusahaan;
    transaksi_val['kode_perusahaan'] = this.kode_perusahaan;
    transaksi_val['konfirm_status'] = false;
    transaksi_val['konfirm_tanggal'] = '';
    transaksi_val['jumlah_bayar'] = Number(transaksi_val.jumlah_bayar);
    if (transaksi_val.periode.month < 10) {
      transaksi_val.periode.month = '0' + transaksi_val.periode.month;
      transaksi_val['periode'] = transaksi_val.periode.year + '-' + transaksi_val.periode.month;
    }else {
      transaksi_val['periode'] = transaksi_val.periode.year + '-' + transaksi_val.periode.month;
    }
    transaksi_val['tanggal_bayar'] = transaksi_val.tanggal_bayar.year + '-' + transaksi_val.tanggal_bayar.month + '-' + transaksi_val.tanggal_bayar.day;
    const transaksi_json = JSON.stringify(transaksi_val);
    console.log(transaksi_json);

    this.loading = true;
    this.httpRequest.httpPost(this.transaksi_url, transaksi_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);
          // console.log(result_msg._id);

          this.rowsFix.forEach(element => {

            const transaksidetail_val = {};
            transaksidetail_val['transaksi'] = result_msg._id;
            transaksidetail_val['perusahaan'] = transaksi_val['perusahaan'];
            transaksidetail_val['kode_perusahaan'] = transaksi_val['kode_perusahaan'];
            transaksidetail_val['peserta'] = element._id;
            transaksidetail_val['paket'] = element.paket._id;
            transaksidetail_val['periode'] = transaksi_val['periode'];
            transaksidetail_val['tanggal_bayar'] = transaksi_val['tanggal_bayar'];
            transaksidetail_val['jumlah_bayar'] = Number(element.paket.jumlah_bayar);
            transaksidetail_val['konfirm_status'] = false;
            transaksidetail_val['konfirm_tanggal'] = '';
            const transaksidetail_json = JSON.stringify(transaksidetail_val);
            console.log(transaksidetail_json);

            this.httpRequest.httpPost(this.transaksidetail_url, transaksidetail_json).subscribe(
              result2 => {
                try {
                  const result_msg2 = JSON.parse(result2._body);
                  // console.log(result_msg2);
                  // console.log(result_msg2._id);

                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                }
              },
              error => {
                console.log(error);
                this.loading = false;
              }
            );
          });

          this.loading = false;
          this.errorMessage.openSuccessSwal('Transaksi created successfully.');

          this.router.navigate(['/perusahaan/transaksi/transaksi-list']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  async showDetail() {

    if (this.data_peserta.length === 0) {

      this.loading = true;

      const peserta_json = {};
      this.httpRequest.httpGet(this.peserta_url, peserta_json).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.data_peserta = result_msg;
            this.rowsFilter = result_msg;
            console.log(this.data_peserta);
            document.getElementById('btnShowDetail').click();

          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
        }
      );

      this.loading = false;
    }else {
      document.getElementById('btnShowDetail').click();
    }

  }

  confirmDetail() {
    const rowsFix = [];
    for (let i = 0; i < this.rowsFilter.length; i++) {
      if (this.rowsFilter[i]['state'] === true) {
        rowsFix.push(this.rowsFilter[i]);
      }
    }
    this.rowsFix = rowsFix;
  }

  check(ev, dataRows) {

    let sumBayar = 0;
    let numPeserta = 0;
    for (let i = 0; i < this.rowsFilter.length; i++) {
      if (this.rowsFilter[i]['id'] === dataRows._id) {
        this.rowsFilter[i]['state'] = ev.target.checked;
      }

      if (this.rowsFilter[i]['state'] === true) {
        sumBayar = sumBayar + this.rowsFilter[i]['paket']['jumlah_bayar'];
        numPeserta = numPeserta + 1;
      }
    }

    this.sumBayar = sumBayar;
    this.numPeserta = numPeserta;
    this.jumlah_bayar = sumBayar.toString();
    console.log(this.rowsFilter);
  }

  checkAll(ev) {
    this.rowsFilter.forEach(x => x.state = ev.target.checked);

    let sumBayar = 0;
    let numPeserta = 0;
    for (let i = 0; i < this.rowsFilter.length; i++) {
      if (this.rowsFilter[i]['state'] === true) {
        sumBayar = sumBayar + this.rowsFilter[i]['paket']['jumlah_bayar'];
        numPeserta = numPeserta + 1;
      }
    }

    this.sumBayar = sumBayar;
    this.numPeserta = numPeserta;
    this.jumlah_bayar = sumBayar.toString();
    console.log(this.rowsFilter);
  }

  isAllChecked() {
    return this.rowsFilter.every(_ => _.state);
  }

}
