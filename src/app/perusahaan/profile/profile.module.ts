import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ProfileComponent } from './profile.component';
import { ProfileRoutes } from './profile.routing';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProfileRoutes),
      SharedModule
  ],
  declarations: [ProfileComponent]
})

export class ProfileModule {}
