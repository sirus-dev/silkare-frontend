import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-transaksi-list',
  templateUrl: './transaksi-list.component.html',
  animations: [fadeInOutTranslate]
})

export class TransaksiListComponent implements OnInit {
  public loading = false;
  private transaksi_url = '/api/penagihans';
  private transaksidetail_url = '/api/penagihandetails';

  public id_perusahaan: string = '5c1092e408950336e0982829';
  public kode_perusahaan: string = '';
  public nama_perusahaan: string = '';

  position: string = 'bottom-right';
  title: string;
  msg: string;
  showClose: boolean = true;
  timeout: number = 5000;
  theme: string = 'bootstrap';
  type: string = 'default';
  closeOther: boolean = false;

  // @ViewChild(DatatableComponent) table: DatatableComponent;
  // loadingIndicator: Boolean = true;
  // reorderable: Boolean = true;
  // showDialog: Boolean = false;

  // columns = [
  //   { name: 'id', sortable: true},
  //   { name: 'id', sortable: true},
  //   { name: 'periode', sortable: true },
  //   { name: 'tanggal_bayar', sortable: true },
  //   { name: 'jumlah_bayar' , sortable: true },
  //   { name: 'konfirm_status' , sortable: true },
  //   { name: 'konfirm_tanggal', sortable: true },
  //   { name: 'id', sortable: true }
  // ];

  rowsFilter = [];
  tempFilter = [];
  rowsDetail = [];
  rowsSelected = {};

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
    this.kode_perusahaan = 'P0001';
    this.nama_perusahaan = 'Perusahaan 1';
  }

  getData(cb) {

    const transaksi_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.transaksi_url, transaksi_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.periode.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    // this.table.offset = 0;
  }

  showDetail(dataRows) {
    this.rowsSelected = dataRows;
    this.loading = true;
    const transaksidetail_json = {};
    this.httpRequest.httpGet(this.transaksidetail_url + '?penagihan=' + dataRows.id, transaksidetail_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.rowsDetail = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
    document.getElementById('btnShowDetail').click();

  }

  showEdit() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowEdit').click();
    }.bind(this), 2000);
  }

  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added removed!');
      }
    };

    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }
}
