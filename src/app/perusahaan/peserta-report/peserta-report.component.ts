import {Component, OnInit, ViewChild} from '@angular/core';
declare const $: any;
declare var Morris: any;
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-peserta-report',
  templateUrl: './peserta-report.component.html'
})

export class PesertaReportComponent implements OnInit {

  dataReport: Array<any>;
  detailPeserta: Array<any>;

  rows = [];
  loadingIndicator = true;
  public loading = false;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  columns = [
    { prop: 'NO' },
    { name: 'NO_KTP', sortable: false },
    { name: 'NO_BPJS', sortable: false },
    { name: 'NAMA' },
    { name: 'JENIS_KELAMIN' },
    { name: 'PEKERJAAN', sortable: false }
  ];

  constructor() {}

  ngOnInit() {
    this.dataReport = [
      {'NO': 1, 'NO_KTP': 32139847958647000, 'NO_BPJS': 34897392856, 'NAMA': 'Raka Maheka A', 'JENIS_KELAMIN': 'L', 'TEMPAT_LAHIR': 'Ciamis', 'TANGGAL_LAHIR': '21/07/1997', 'AGAMA': 'Islam', 'PEKERJAAN': 'Karyawan', 'NO_TELEPON': 628283745678, 'ALAMAT': 'Ciamis'},
      {'NO': 2, 'NO_KTP': 49536593465439400, 'NO_BPJS': 43534859347, 'NAMA': 'Amelia', 'JENIS_KELAMIN': 'P', 'TEMPAT_LAHIR': 'Padang', 'TANGGAL_LAHIR': '15/04/1995', 'AGAMA': 'Islam', 'PEKERJAAN': 'Karyawan', 'NO_TELEPON': 6282837283760, 'ALAMAT': 'Padang'},
      {'NO': 3, 'NO_KTP': 66933338972231800, 'NO_BPJS': 84598729638, 'NAMA': 'Jerome', 'JENIS_KELAMIN': 'L', 'TEMPAT_LAHIR': 'Bandung', 'TANGGAL_LAHIR': '28/8/1996', 'AGAMA': 'Kristen', 'PEKERJAAN': 'Karyawan', 'NO_TELEPON': 6282435663444, 'ALAMAT': 'Bandung'},
      {'NO': 4, 'NO_KTP': 84330084479024200, 'NO_BPJS': 49857438756, 'NAMA': 'Lutfi', 'JENIS_KELAMIN': 'L', 'TEMPAT_LAHIR': 'Cikarang', 'TANGGAL_LAHIR': '10/10/1975', 'AGAMA': 'Islam', 'PEKERJAAN': 'Karyawan', 'NO_TELEPON': 6287845634673, 'ALAMAT': 'Cikarang'} ];
      this.rows = this.dataReport;
  }

  showDetail(data) {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      this.detailPeserta = [data];
      console.log(this.detailPeserta);
    }.bind(this), 2000);
  }
}
