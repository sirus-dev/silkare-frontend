import { Routes } from '@angular/router';

import { PesertaListComponent } from './peserta-list.component';

export const PesertaListRoutes: Routes = [{
  path: '',
  component: PesertaListComponent,
  data: {
    breadcrumb: 'Daftar Anggota',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
