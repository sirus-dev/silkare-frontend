import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-peserta-list',
  templateUrl: './peserta-list.component.html',
  animations: [fadeInOutTranslate]
})

export class PesertaListComponent implements OnInit {
  public loading = false;
  private peserta_url = '/api/pesertas';
  private perusahaan_url = '/api/perusahaans';
  private klinik_url = '/api/kliniks';
  private rumahsakit_url = '/api/rumahsakits';

  @ViewChild(DatatableComponent) table: DatatableComponent;
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  columns = [
    { prop: '_id' },
    { name: 'no_ktp', sortable: false },
    { name: 'no_bpjs', sortable: false },
    { name: 'nama' },
    { name: 'jenis_kelamin' },
    { name: 'pekerjaan', sortable: false }
  ];

  rowsFilter = [];
  tempFilter = [];
  rowsPerusahaan = [];
  rowsKlinik = [];
  rowsRumahSakit = [];

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
    this.getData((data) => {


      data.forEach(element => {
        console.log(element.nama);
        let i = 0;
        // for (i = 0; i <= element.transaksidetail.length; i++) {
        //   console.log(element.transaksidetail[i]['periode']);
        //   console.log(element.transaksidetail[i]['konfirm_status']);
        // }
        element.transaksidetail.forEach(trans => {
          if (trans.konfirm_status === true) {
            element.last_active = trans.periode;
          }
          if (i === element.transaksidetail.length - 1) {
            element.last_transaksi_periode = trans.periode;
            element.last_transaksi_status = trans.konfirm_status;
          }
          i = i + 1;

           // Do whatever you want
           // You can access any field like that : category.yourField
        });
      });

      this.tempFilter = [...data];
      this.rowsFilter = data;
      // console.log
    });

    this.getDataPerusahaan((data) => {
      this.rowsPerusahaan = data;
    });
    this.getDataKlinik((data) => {
      this.rowsKlinik = data;
    });
    this.getDataRumahSakit((data) => {
      this.rowsRumahSakit = data;
    });
  }

  ngOnInit() {
  }

  getData(cb) {

    const peserta_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.peserta_url, peserta_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataPerusahaan(cb) {

    const perusahaan_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.perusahaan_url + '?is_deleted=false&_sort=nama:ASC', perusahaan_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKlinik(cb) {

    const klinik_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.klinik_url + '?is_deleted=false&_sort=nama:ASC', klinik_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataRumahSakit(cb) {

    const rumahsakit_json = {};
    this.loading = true;
    this.httpRequest.httpGet(this.rumahsakit_url + '?is_deleted=false&_sort=nama:ASC', rumahsakit_json).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  deleteData(id, status) {
    const peserta_val = {};
    peserta_val['_id'] = id;
    peserta_val['status_aktif_pekerja'] = !status;
    const peserta_json = JSON.stringify(peserta_val);
    // console.log(peserta_json);

    this.loading = true;
    this.httpRequest.httpPut(this.peserta_url + '/' + id, peserta_json).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.getData((data) => {
            this.tempFilter = [...data];
            this.rowsFilter = data;
          });

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.nama.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  changePerusahaan(event) {
    const val = event.target.value.toLowerCase();

    if (val === '-') {
      this.rowsFilter = this.tempFilter;
    }else {
      const temp = this.tempFilter.filter(function(d) {
        return d.perusahaan.nama_perusahaan.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.rowsFilter = temp;
    }

    this.table.offset = 0;
  }

  changeKlinik(event) {
    const val = event.target.value.toLowerCase();

    if (val === '-') {
      this.rowsFilter = this.tempFilter;
    }else {
      const temp = this.tempFilter.filter(function(d) {
        return d.klinik.nama_klinik.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.rowsFilter = temp;
    }

    this.table.offset = 0;
  }

  changeRumahSakit(event) {
    const val = event.target.value.toLowerCase();

    if (val === '-') {
      this.rowsFilter = this.tempFilter;
    }else {
      const temp = this.tempFilter.filter(function(d) {
        return d.rumahsakit.nama_rumah_sakit.toLowerCase().indexOf(val) !== -1 || !val;
      });
      this.rowsFilter = temp;
    }

    this.table.offset = 0;
  }

  showConfirmDelete(event, id, status) {
    swal({
      title: 'Konfirmasi Ubah Status',
      text: 'Anda yakin mengubah status peserta ini?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteData(id, status);
        swal(
            'Info!',
            'Status peserta berhasil diubah.',
            'success'
        );
      }
    });
  }


  showDetail() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowDetail').click();
    }.bind(this), 2000);
  }

  showEdit() {
    this.loading = true;
    setTimeout(function() {
      this.loading = false;
      document.getElementById('btnShowEdit').click();
    }.bind(this), 2000);
  }

}
