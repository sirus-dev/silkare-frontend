import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PesertaListComponent } from './peserta-list.component';
import { PesertaListRoutes } from './peserta-list.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PesertaListRoutes),
      SharedModule,
      NgxDatatableModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PesertaListComponent]
})

export class PesertaListModule {}
