import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { SessionService } from './shared/service/session.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet><app-spinner></app-spinner></router-outlet>'
})
export class AppComponent {

  constructor( private router: Router, private session: SessionService) {

    router.events.forEach((event: NavigationEvent) => {

      // After Navigation
      // if (event instanceof NavigationEnd) {

      // Berfore Navigation
      if (event instanceof NavigationStart) {
        const url = event.url;
        const routing = url.split('/');
        const role = this.session.getRole();

        // console.log('call every change route');
        // console.log('url : ' + url);
        // console.log('routing : ' + routing);
        // console.log('islogin : ' + this.session.isLoggedIn());
        // console.log('check access : ' + this.session.checkAccess(url));
        // console.log('tokenExpired : ' + this.session.getTokenExpired());

        if (routing[1] === '') {
          this.router.navigate(['/' + role]);

        } else if ((routing[1] === 'auth') && (routing[2] === 'logout')) {
          console.log('logouting');

        } else if (routing[1] === 'error') {
          console.log('error page');

        } else {
          if (this.session.isLoggedIn()) {
            if (!this.session.checkAccess(url)) {
              console.log('dont have access, redirect dashboard');
              this.router.navigate(['/error/403']);
            }
          } else {
            if (routing[1] !== 'auth') {
              console.log('session false, redirect login');
              this.router.navigate(['/auth/login']);
            }
          }
        }

      }
    });
  }

}
